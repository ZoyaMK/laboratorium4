package lab4Zad5.la4Zad5;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class La4Zad5Application {

	public static void main(String[] args) {
		SpringApplication.run(La4Zad5Application.class, args);
	}

}
