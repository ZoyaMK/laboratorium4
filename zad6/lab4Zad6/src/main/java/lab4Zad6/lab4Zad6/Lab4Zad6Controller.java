package lab4Zad6.lab4Zad6;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller            // <1>
public class Lab4Zad6Controller {

  @GetMapping("/")     // <2>
  public String home() {
    return "home";     // <3>
  }

}