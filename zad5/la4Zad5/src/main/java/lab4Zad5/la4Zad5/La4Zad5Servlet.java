package lab4Zad5.la4Zad5;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;


public class La4Zad5Servlet extends HttpServlet {
 
    private String message;
 
    public void init() throws ServletException {
       
       message = "Witam Czlonka partii";
    }
 
    public void doGet(HttpServletRequest request, HttpServletResponse response)
       throws ServletException, IOException {
       
       response.setContentType("text/html");
 
       PrintWriter out = response.getWriter();
       out.println("<h1>" + message + "</h1>");
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {

    }

    public void doAction(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {

    }
 
    public void destroy() {

    }
 }