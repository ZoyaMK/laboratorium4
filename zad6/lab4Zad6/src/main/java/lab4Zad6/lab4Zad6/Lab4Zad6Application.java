package lab4Zad6.lab4Zad6;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Lab4Zad6Application {

	public static void main(String[] args) {
		SpringApplication.run(Lab4Zad6Application.class, args);
	}

}
